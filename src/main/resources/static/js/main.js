

var arr = ['hello', 14, 'phrase'];

let counter = 0;

while (counter < arr.length) {
    console.log(arr[counter]);
    counter ++;
}

var andrii = {
    name : 'Andrii',
    age : 31,
    getAge : function() {return andrii.age}
}

// var getAge = function () {return andrii.age};
console.log(andrii.getAge());

var vasyl = {
    constructor : function() {
        vasyl.age = 3
    }
}

console.log(vasyl.age);

package com.bh.api.cats.apicats.services;

import com.bh.api.cats.apicats.api.v1.CatDTO;
import com.bh.api.cats.apicats.api.v1.CatMapper;
import com.bh.api.cats.apicats.model.Cat;
import com.bh.api.cats.apicats.repositories.CatRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CatServiceImpl implements CatService {

    private final CatMapper catMapper;
    private final CatRepository catRepository;

    public CatServiceImpl(CatMapper catMapper, CatRepository catRepository) {
        this.catMapper = catMapper;
        this.catRepository = catRepository;
    }

    @Override
    public List<CatDTO> findAllCats() {

        return catRepository.findAll()
                .stream()
                .map(cat -> {
                    CatDTO catDTO = catMapper.catToCatDTO(cat);
                    catDTO.setUrl("/api/v1/cats/" + cat.getId() );
                    return catDTO;
                })
                .collect(Collectors.toList());
    }

    @Override
    public CatDTO findCatById(Long id) {

        return catRepository.findById(id)
                .map(catMapper ::catToCatDTO)
                .orElseThrow(RuntimeException::new);
    }

    @Override
    public CatDTO createNewCat(CatDTO catDTO) {

        Cat cat = catMapper.catDtoToCat(catDTO);

        Cat savedCat = catRepository.save(cat);

        CatDTO returnedDto = catMapper.catToCatDTO(savedCat);

        returnedDto.setUrl("/api/v1/cats/" + savedCat.getId());

        return returnedDto;
    }

    private CatDTO saveAndReturnDTO(Cat cat) {

        Cat savedCat = catRepository.save(cat);

        CatDTO returnDto = catMapper.catToCatDTO(savedCat);

        returnDto.setUrl("/api/v1/cats/" + savedCat.getId());

        return returnDto;
    }

    @Override
    public CatDTO saveCatByDto(long id, CatDTO catDTO) {

        Cat cat = catMapper.catDtoToCat(catDTO);

        cat.setId(id);

        return saveAndReturnDTO(cat);
    }

    @Override
    public CatDTO patchCat(Long id, CatDTO catDTO) {

        return catRepository.findById(id).map(cat -> {

            if (cat.getName() != null) cat.setName(catDTO.getName());
            if (cat.getAge() != 0) cat.setAge(catDTO.getAge());

            CatDTO returnCat = catMapper.catToCatDTO(catRepository.save(cat));

            returnCat.setUrl("/api/v1/cats/" + id);

            return returnCat;

        }).orElseThrow(RuntimeException::new);
    }

    @Override
    public void deleteCatById(Long id) {

        catRepository.deleteById(id);
    }
}

package com.bh.api.cats.apicats.api.v1;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class CatDTO {

    private String name;
    private int age;
    private String color;

    @JsonProperty("cat_url")
    private String url;
}

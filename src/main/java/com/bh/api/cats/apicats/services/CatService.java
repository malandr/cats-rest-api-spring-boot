package com.bh.api.cats.apicats.services;

import com.bh.api.cats.apicats.api.v1.CatDTO;
import org.springframework.stereotype.Service;

import java.util.List;

public interface CatService {

    List<CatDTO> findAllCats();

    CatDTO findCatById(Long id);

    CatDTO createNewCat(CatDTO catDTO);

    CatDTO saveCatByDto(long id, CatDTO catDTO);

    CatDTO patchCat(Long id, CatDTO catDTO);

    void deleteCatById(Long id);
}

package com.bh.api.cats.apicats.api.v1;

import com.bh.api.cats.apicats.model.Cat;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface CatMapper {

    CatMapper INSTANCE = Mappers.getMapper(CatMapper.class);

    CatDTO catToCatDTO(Cat cat);

    Cat catDtoToCat(CatDTO catDTO);
}

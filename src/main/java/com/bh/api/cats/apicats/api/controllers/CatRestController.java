package com.bh.api.cats.apicats.api.controllers;

import com.bh.api.cats.apicats.api.v1.CatDTO;
import com.bh.api.cats.apicats.api.v1.CatListDTO;
import com.bh.api.cats.apicats.services.CatService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping(CatRestController.BASE_URL)
public class CatRestController {

    static final String BASE_URL = "/api/v1/cats";

    private final CatService catService;

    @Autowired
    public CatRestController(CatService catService) {
        this.catService = catService;
    }

//    @GetMapping(produces = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE})
//    @ResponseStatus(HttpStatus.OK)
//    public List<Cat> getAllCats() {
//
//        return catService.getAllCats();
//    }

    @GetMapping(produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<CatListDTO> getListOfCats() {

        return new ResponseEntity<CatListDTO>(new CatListDTO(catService.findAllCats()),
                HttpStatus.OK);
    }

    @PostMapping(consumes = {MediaType.APPLICATION_JSON_VALUE}
    )
    public ResponseEntity<CatDTO> createNewCat(@RequestBody @Valid CatDTO catDTO){

        return new ResponseEntity<CatDTO>(catService.createNewCat(catDTO),
                HttpStatus.CREATED);
    }

    @GetMapping(path = "{id}", produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<CatDTO> getCatById(@PathVariable Long id) {

        return new ResponseEntity<CatDTO>(catService.findCatById(id), HttpStatus.OK);
    }

    @PutMapping({"{id}"})
    public ResponseEntity<CatDTO> changeCat(@PathVariable long id, @RequestBody CatDTO catDTO) {

        return new ResponseEntity<CatDTO>(catService.saveCatByDto(id, catDTO), HttpStatus.OK);
    }

    @PatchMapping({"{id}"})
    public ResponseEntity<CatDTO> patchCat(@PathVariable Long id, @RequestBody CatDTO catDTO){

        return new ResponseEntity<CatDTO>(catService.patchCat(id, catDTO), HttpStatus.OK);
    }

    @DeleteMapping({"{id}"})
    public ResponseEntity<Void> deleteCat(@PathVariable long id) {

        catService.deleteCatById(id);

        return new ResponseEntity<Void>(HttpStatus.OK);
    }

}

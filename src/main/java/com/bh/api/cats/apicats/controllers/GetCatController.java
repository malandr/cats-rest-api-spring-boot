package com.bh.api.cats.apicats.controllers;

import com.bh.api.cats.apicats.api.v1.CatDTO;
import com.bh.api.cats.apicats.model.Cat;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletRequest;

@Controller
public class GetCatController {

    @RequestMapping("/cat-from-url")
    public String getCat(HttpServletRequest request, Model model) {

        RestTemplate restTemplate = new RestTemplate();

        Cat catFromUrl = restTemplate.getForObject("http://localhost:8080/api/v1/cats/1", Cat.class);

        Cat[] catsFromUrl = restTemplate.getForObject("http://localhost:8080/api/v1/cats", Cat[].class);

        model.addAttribute("catFromUrl", catFromUrl);
        model.addAttribute("catsFromUrl", catsFromUrl);

        return "cat-url";
    }

    private String getPath(HttpServletRequest request) {

        int port = request.getServerPort();

        StringBuilder result = new StringBuilder();

        result.append(request.getScheme())
                .append("://")
                .append(request.getServerName());

        if ((request.getScheme().equals("http") && port != 80) || (request.getScheme().equals("https") && port != 443)) {

            result.append(':')
                    .append(port);
        }

        result.append(request.getContextPath());

//        if (pageUrl != null && pageUrl.length() > 0) {
//
//            if (!pageUrl.startsWith("/")) {
//
//                result.append("/");
//            }
//
//            result.append(pageUrl);
//        }

        return result.toString();
    }
}


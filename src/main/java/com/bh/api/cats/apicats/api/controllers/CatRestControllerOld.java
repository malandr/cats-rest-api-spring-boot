//package com.bh.api.cats.apicats.api.controllers;
//
//import com.bh.api.cats.apicats.model.Cat;
//import com.bh.api.cats.apicats.services.CatService;
//import org.springframework.http.MediaType;
//import org.springframework.web.bind.annotation.*;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.http.HttpStatus;
//
//import javax.validation.Valid;
//import java.util.List;
//
//@RestController
//@RequestMapping(CatRestControllerOld.BASE_URL)
//public class CatRestControllerOld {
//
//    static final String BASE_URL = "/api/v1/cats";
//
//    private final CatService catService;
//
//    public CatRestControllerOld(CatService catService) {
//        this.catService = catService;
//    }
//
//    @GetMapping(produces = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE})
//    @ResponseStatus(HttpStatus.OK)
//    public List<Cat> getAllCats() {
//
//        return catService.getAllCats();
//    }
//
//    @GetMapping(path = "{id}", produces = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE})
//    @ResponseStatus(HttpStatus.OK)
//    public Cat getCat(@PathVariable long id) {
//
//        return catService.findCatById(id);
//    }
//
//    @PostMapping(produces = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE},
//            consumes = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE})
//    @ResponseStatus(HttpStatus.CREATED)
//    public Cat createCat(@Valid @RequestBody Cat cat) {
//
//        return catService.createCat(cat);
//    }
//
//    @PutMapping("{id}")
//    public Cat editCat(@PathVariable long id, @RequestBody Cat cat) {
//
//        return catService.changeCat(id, cat);
//    }
//
//
//
//}

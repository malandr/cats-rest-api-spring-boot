package com.bh.api.cats.apicats.api.v1;
/*
Nov 13, 2019
 */
import com.bh.api.cats.apicats.model.Cat;

public class CatMapperImpl implements CatMapper{

    @Override
    public CatDTO catToCatDTO(Cat cat) {

//        if ( cat == null ) {
//            return null;
//        }

        CatDTO catDTO = new CatDTO();

        catDTO.setName( cat.getName() );
        catDTO.setAge( cat.getAge() );
        catDTO.setColor( cat.getColor() );
        catDTO.setUrl("/api/va/cats/" + cat.getId());

        return catDTO;
    }

    @Override
    public Cat catDtoToCat(CatDTO catDTO) {

//        if ( catDTO == null ) {
//            return null;
//        }

        Cat cat = new Cat();

        cat.setName( catDTO.getName() );
        cat.setAge( catDTO.getAge() );
        cat.setColor( catDTO.getColor() );

        return cat;
    }
}

package com.bh.api.cats.apicats.repositories;

import com.bh.api.cats.apicats.model.Cat;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CatRepository extends JpaRepository<Cat, Long> {

}

package com.bh.api.cats.apicats.controllers;


import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class JQController {

    @RequestMapping("/jq-test")
    public String getJqPage(Model model) {

        return "jq-test";
    }
}

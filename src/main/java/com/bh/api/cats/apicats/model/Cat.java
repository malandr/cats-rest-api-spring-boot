package com.bh.api.cats.apicats.model;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Data
public class Cat {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;

    @NotNull(message = "Name cannot be empty")
    @Size(min = 1, max = 30)
    private String name;

    @NotNull
    private int age;

    @NotNull
    private String color;
}

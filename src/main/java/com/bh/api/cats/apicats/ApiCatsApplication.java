package com.bh.api.cats.apicats;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiCatsApplication {

	public static void main(String[] args) {

		SpringApplication.run(ApiCatsApplication.class, args);
	}

}

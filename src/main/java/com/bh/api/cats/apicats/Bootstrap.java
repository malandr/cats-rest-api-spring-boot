package com.bh.api.cats.apicats;

import com.bh.api.cats.apicats.model.Cat;
import com.bh.api.cats.apicats.repositories.CatRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class Bootstrap implements CommandLineRunner {

    private final CatRepository catRepository;

    public Bootstrap(CatRepository catRepository) {
        this.catRepository = catRepository;
    }

    private void loadCats() {

        Cat liza = new Cat();
        liza.setId(1L);
        liza.setName("Liza");
        liza.setAge(10);
        liza.setColor("Three-colored");
        catRepository.save(liza);

        Cat snizhok = new Cat();
        snizhok.setId(2L);
        snizhok.setName("Snizhok");
        snizhok.setAge(9);
        snizhok.setColor("White");
        catRepository.save(snizhok);
    }

    @Override
    public void run(String... args) throws Exception {
        loadCats();
    }
}

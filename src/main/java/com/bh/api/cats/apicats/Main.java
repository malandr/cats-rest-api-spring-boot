package com.bh.api.cats.apicats;

import com.bh.api.cats.apicats.model.Cat;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;

public class Main {

    private static String readUrl(String urlString) throws Exception {

        BufferedReader reader = null;

        try {

            URL url = new URL(urlString);

            reader = new BufferedReader(new InputStreamReader(url.openStream()));

            StringBuffer buffer = new StringBuffer();

            int read;
            char[] chars = new char[1024];

            while ((read = reader.read(chars)) != -1)

                buffer.append(chars, 0, read);

            return buffer.toString();

        } finally {
            if (reader != null)
                reader.close();
        }
    }

    public static void main(String[] args) throws Exception {


//        String jsonString = "{\"name\":\"Mahesh\", \"age\":21}";

        String jsonString = readUrl ("http://localhost:8080/api/v1/cats/1");

        GsonBuilder builder = new GsonBuilder();

        builder.setPrettyPrinting();

        Gson gson = builder.create();

        Cat cat = gson.fromJson(jsonString, Cat.class);

        System.out.println(cat);

        jsonString = gson.toJson(cat);
        System.out.println(jsonString);
    }
}